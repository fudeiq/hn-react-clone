import React from "react";
import HNElement from "./../HNElement/HNElement";
import Nav from "./../Nav/Nav";
import "./../../helpers/Common.scss";
import { getItems } from "./../../helpers/Helpers";

class Best extends React.Component {
  constructor(props) {
    super(props);
    this.state = { elements: [] };
  }
  componentDidMount() {
    getItems("beststories", this);
  }
  render() {
    const listNews = this.state.elements.map(e => (
      <HNElement data={e} key={e.id} />
    ));
    return (
      <div className="content">
        <Nav />
        {listNews}
      </div>
    );
  }
}

export default Best;
