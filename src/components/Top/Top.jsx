import React from "react";
import HNElement from "./../HNElement/HNElement";
import Nav from "./../Nav/Nav";
import "./../../helpers/Common.scss";

class Top extends React.Component {
  constructor(props) {
    super(props);
    this.state = { elements: [] };
    fetch("https://hacker-news.firebaseio.com/v0/topstories.json")
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.onStoriesLoad(data);
      });
  }
  onStoriesLoad(data) {
    data.slice(0, 50).forEach(i => {
      fetch(`https://hacker-news.firebaseio.com/v0/item/${i}.json`)
        .then(res => {
          return res.json();
        })
        .then(data => {
          this.setState(state => ({
            elements: [...state.elements, data]
          }));
        });
    });
  }
  render() {
    const listNews = this.state.elements.map(e => (
      <HNElement data={e} key={e.id} />
    ));
    return (
      <div className="content">
        <Nav />
        <div>{listNews}</div>
      </div>
    );
  }
}

export default Top;
