import React from "react";
import { Link } from "react-router-dom";
import { timeSince } from "./../../helpers/Helpers";
import "./style.scss";

class HNElement extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { data } = this.props;
    return (
      <div className="el">
        <div className="cont">
          <div className="top">
            <a className="url" href={data.url} target="_blank">
              <b className="title">{data.title}</b>
            </a>
          </div>
          <div className="bottom">
            <span className="score ib">
              <b>{data.score}</b>
              <span>{Number(data.score) == 1 ? " point" : " points"}</span>
            </span>
            <span>
              <Link className="by ib" to={`/user/${data.by}`}>
                {data.by}
              </Link>
            </span>
            <span className="time">{timeSince(Number(data.time))} ago</span>
            {data.url && (
              <span className="surl ib">({data.url.split("/")[2]})</span>
            )}
            <Link to={`/article/${data.id}`} className="comments ib">
              {data.descendants}
              {Number(data.descendants) == 1 ? " comment" : " comments"}
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default HNElement;
