import React from "react";
import "./style.scss";

class Article extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: {} };
    this.getArticle();
    this.getArticle = this.getArticle.bind(this);
  }
  getArticle() {
    fetch(
      `https://hacker-news.firebaseio.com/v0/item/${
        this.props.match.params.id
      }.json`
    )
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState({ data });
      });
  }
  render() {
    return (
      <div>
        <h1 className="title">{this.state.data.title}</h1>
      </div>
    );
  }
}

export default Article;
