import React from "react";
import "./style.scss";

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: {} };
    this.getUser();
    this.getUser = this.getUser.bind(this);
  }
  getUser() {
    fetch(
      `https://hacker-news.firebaseio.com/v0/user/${
        this.props.match.params.id
      }.json`
    )
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState({ data });
      });
  }
  render() {
    // TODO: complete
    // handle if user was not found
    // handle loading animation or some shit
    console.log(this.state.data);
    return (
      <div className="user">
        <p>user: {this.state.data.id}</p>
        <p>karma: {this.state.data.karma}</p>
        <p>
          created at:
          {new Date(this.state.data.created * 1000).toGMTString()}
        </p>
        {this.state.data.submitted && (
          <p>submitions: {this.state.data.submitted.length}</p>
        )}
        <div>
          about:
          <div dangerouslySetInnerHTML={{ __html: this.state.data.about }} />
        </div>
      </div>
    );
  }
}

export default User;
