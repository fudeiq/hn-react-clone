import React from "react";
import "./style.scss";

class ErrorPage extends React.Component {
  render() {
    return <h1 className="info">404 not found</h1>;
  }
}

export default ErrorPage;
