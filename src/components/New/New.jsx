import React from "react";
import HNElement from "./../HNElement/HNElement";
import Nav from "./../Nav/Nav";
import "./../../helpers/Common.scss";

class New extends React.Component {
  constructor(props) {
    super(props);
    this.state = { elements: [] };
  }
  onStoriesLoad(data) {
    data.slice(0, 50).forEach(i => {
      fetch(`https://hacker-news.firebaseio.com/v0/item/${i}.json`)
        .then(res => {
          return res.json();
        })
        .then(data => {
          this.setState(state => ({
            elements: [...state.elements, data]
          }));
        });
    });
  }
  componentDidMount() {
    fetch("https://hacker-news.firebaseio.com/v0/newstories.json")
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.onStoriesLoad(data);
      });
  }
  render() {
    const listNews = this.state.elements.map(e => (
      <HNElement data={e} key={e.id} />
    ));
    return (
      <div className="content">
        <Nav />
        {listNews}
      </div>
    );
  }
}

export default New;
