import React from "react";
import "./style.scss";
import { Link } from "react-router-dom";

class Nav extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="nav">
        <Link to="/" className="link">
          TOP
        </Link>
        <Link to="/new" className="link">
          NEW
        </Link>
        <Link to="/best" className="link">
          BEST
        </Link>
      </div>
    );
  }
}

export default Nav;
