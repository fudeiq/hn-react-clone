import React from "react";
import ReactDOM from "react-dom";

import Top from "./components/Top/Top";
import New from "./components/New/New";
import Best from "./components/Best/Best";
import User from "./components/User/User";
import ErrorPage from "./components/ErrorPage/ErrorPage";
import Article from "./components/Article/Article";

import { BrowserRouter, Route, Switch } from "react-router-dom";

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Switch>
            <Route path="/" component={Top} exact />
            <Route path="/user/:id" component={User} exact />
            <Route path="/article/:id" component={Article} exact />
            <Route path="/new" component={New} exact />
            <Route path="/best" component={Best} exact />
            <Route component={ErrorPage} />
          </Switch>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
